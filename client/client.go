package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

var conn net.Conn

func main() {
	//conn, _ = net.Dial("tcp", "127.0.0.1:41503")  //LOCALHOST
	conn, _ = net.Dial("tcp", "10.192	.145.245:40693") //MIN IP ADDRESSE PÅ EDUROAM
	defer conn.Close()
	reader := bufio.NewReader(os.Stdin)
	connReader := bufio.NewReader(conn)
	for {
		fmt.Print("> ")
		text, err := reader.ReadString('\n')
		if text == "quit\n" {
			return
		}
		fmt.Fprintf(conn, text)
		msg, err := connReader.ReadString('\n')
		if err != nil {
			return
		}
		// Windows uses \r\n as a return character, Trimspace removes the extra '\r' char
		msg = strings.TrimSpace(msg)
		fmt.Println("From server: " + msg)
	}
}

/*  //UDP STUFF:
package main


import (
	"net"
	"strconv"
	"time"
)

func main() {
	ServerAddr, _ := net.ResolveUDPAddr("udp", "127.0.0.1:10001")
	LocalAddr, _ := net.ResolveUDPAddr("udp", "127.0.0.1:0")
	conn, _ := net.DialUDP("udp", LocalAddr, ServerAddr)
	defer conn.Close()
	i := 0
	for {
		i++
		msg := strconv.Itoa(i)
		conn.Write([]byte(msg))
		println("sending: " + msg)
		time.Sleep(time.Second * 1)
	}
}
*/
