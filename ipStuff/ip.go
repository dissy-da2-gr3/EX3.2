package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
)

func printIP() {

	name, _ := os.Hostname()
	fmt.Println("Name: " + name)

	addresses, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println("Error is:", err)
	}

	for _, address := range addresses {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				fmt.Println("Non-loopback IP is: ", ipnet.IP.String())
			}
		}
		if ipnet, ok := address.(*net.IPNet); ok && ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				fmt.Println("Loopback IP is: ", ipnet.IP.String())
			}
		}
	}

	/*
		addrs, _ := net.LookupHost(name)
		for indx, addr := range addrs {
			fmt.Println("Address number " + strconv.Itoa(indx) + ": " + addr)
		}*/
}

func main() {
	printIP()
	port := 10001
	ServerAddr, _ := net.ResolveUDPAddr("udp", ":port")
	ServerConn, _ := net.ListenUDP("udp", ServerAddr)
	defer ServerConn.Close()
	println("Listening for UDP stuff on port " + strconv.Itoa(port) + "...")

	buf := make([]byte, 1024)
	for {
		n, addr, err := ServerConn.ReadFromUDP(buf)
		fmt.Println("Received ", string(buf[0:n]), " from ", addr)
		if err != nil {
			fmt.Println("Error: ", err)
		}
	}
}
