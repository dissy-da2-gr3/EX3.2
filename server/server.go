package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func handleConnection(conn net.Conn) {
	defer conn.Close()
	myEnd := conn.LocalAddr().String()
	otherEnd := conn.RemoteAddr().String()
	reader := bufio.NewReader(conn)
	for {
		msg, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Ending session with " + otherEnd)
			return
		} else {
			fmt.Print("From " + otherEnd + " to " + myEnd + ": " + string(msg))
			titlemsg := strings.Title(msg)
			conn.Write([]byte(titlemsg))
		}
	}
}
func main() {
	ln, _ := net.Listen("tcp", "10.192.145.245:")
	defer ln.Close()
	print("Here's my port: ")
	print(ln.Addr().(*net.TCPAddr).Port)
	print("... And here's my IP: ")
	println(ln.Addr().(*net.TCPAddr).IP.String())
	for {
		fmt.Println("Listening for connection...")
		conn, _ := ln.Accept()
		fmt.Println("Got a connection...")
		go handleConnection(conn)
	}
}

/* //UDP SERVER STUFF:
import (
	"fmt"
	"net"
)

func main() {
	//port := 10001
	ServerAddr, _ := net.ResolveUDPAddr("udp", "127.0.0.1:10001")
	ServerConn, _ := net.ListenUDP("udp", ServerAddr)
	defer ServerConn.Close()
	println("Listening for UDP stuff on port 10001...")

	buf := make([]byte, 1024)
	for {
		n, addr, err := ServerConn.ReadFromUDP(buf)
		fmt.Println("Received ", string(buf[0:n]), " from ", addr)
		if err != nil {
			fmt.Println("Error: ", err)
		}
	}
}
*/
